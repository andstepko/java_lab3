package ua.nure.stepko.Task3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Problem6 {
	private static final String MAILS_FILE_NAME =  "mails.txt";
	private static final String GROUPS_FILE_NAME =  "groups.txt";
	private static final String USERS_FILE_NAME =  "users.txt";
	private static final String ENCODING = "Cp1251";
	
	public static void main(String[] args) throws IOException {
		Reader mailsIn = new InputStreamReader(new FileInputStream(new File(MAILS_FILE_NAME)), ENCODING); 
		Reader groupsIn = new InputStreamReader(new FileInputStream(new File(GROUPS_FILE_NAME)), ENCODING);
		Writer out = new OutputStreamWriter(new FileOutputStream(new File(USERS_FILE_NAME)), ENCODING);
		
		filter(mailsIn, groupsIn, out);
		mailsIn.close();
		groupsIn.close();
		out.close();
	}
	
	public static void filter(Reader mailsReader, Reader groupsReader, Writer usersWriter) throws IOException{
		String mails = readerToString(mailsReader);
		String groups = readerToString(groupsReader);
		
		String loginsRegexp = "(?<=\\s)[\\p{javaUpperCase}|\\p{javaLowerCase}|0-9]+(?=;)";
		String mailsRegexp = "(?<=\\s[\\p{javaUpperCase}|\\p{javaLowerCase}|0-9]{1,45};).+";
		String groupsRegexp = "(?<=\\s[\\p{javaUpperCase}|\\p{javaLowerCase}|0-9]{1,45};).+";
		
		List<String> mailLoginsList = parseText(loginsRegexp, mails);
		List<String> groupsLoginsList = parseText(loginsRegexp, groups);
		List<String> mailsList = parseText(mailsRegexp, mails);
		List<String> groupsList = parseText(groupsRegexp, groups);
		
		usersWriter.write("Login;Email;Group" + System.lineSeparator());
		System.out.println("Login;Email;Group");
		
		for(int i = 1; i < mailLoginsList.size(); i++){
			int index = groupsLoginsList.indexOf(mailLoginsList.get(i));
			
			if(index != -1){
				// Add the record into output..
				StringBuilder result = new StringBuilder();
				result.append(mailLoginsList.get(i) + ";");
				result.append(mailsList.get(i) + ";");
				result.append(groupsList.get(index));
				
				usersWriter.write(result.toString() + System.lineSeparator());
				System.out.println(result.toString());
			}
		}
	}
	
	private static String readerToString(Reader reader) throws IOException{
		StringBuilder sb = new StringBuilder();
		int i;
		char c;
		
		i = reader.read();
		c = (char)i;
		while(i != -1){
			sb.append(c);
			i = reader.read();
			c = (char)i;
		}
		return sb.toString();
	}

	private static List<String> parseText(String regexp, String text){
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(text);
		List<String> result = new ArrayList<String>();
		
		while(matcher.find()){
			result.add(matcher.group());
		}
		return result;
	}
}
