package ua.nure.stepko.Task3;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Demo {
	private static final InputStream STD_IN = System.in;
	private static final String ENCODING = "Cp1251";
	
	public static void main(String[] args) throws IOException {
		System.out.println("=========================== PART1");
		Problem1.main(args);
		
		System.out.println("=========================== PART2");
		Problem2.main(args);
		
		System.out.println("=========================== PART3");
		System.setIn(new ByteArrayInputStream("char\nString\nint\ndouble".getBytes(ENCODING)));
		Problem3.main(args);
		System.setIn(STD_IN);
		
		System.out.println("=========================== PART4");
		Problem4.main(args);
		
		System.out.println("=========================== PART5");
		System.setIn(new ByteArrayInputStream("table ru\ntable en\napple ru".getBytes(ENCODING)));
		Problem5.main(args);
		System.setIn(STD_IN);
		
		System.out.println("=========================== PART6");
		Problem6.main(args);
		
		System.out.println("=========================== PART7");
		Problem7.main(args);
	}
}
