package ua.nure.stepko.Task3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Problem7 {
	private static final String ENCODING = "Cp1251";
	private static final int LESSONS_TO_PRINT = 10;
	private static final String SEPARATOR = "----------------";
	private static String fileName = "TimeTable_14_04_2015.csv";

	public static void main(String[] args) throws IOException {
		if (args.length == 3) {
			fileName = args[0];
		}

		String dateRegExp = "(?<=\")[\\d]{2}.[\\d]{2}.[\\d]{4}(?=\")";
		String timeRegExp = "(?<=\")[\\d]{2}:[\\d]{2}:[\\d]{2}(?=\")";
		String subjectRegExp = "(?m)(?<=\n\")[\\p{javaLowerCase}|\\p{javaUpperCase}|_]+(?= )";
		String lessonTypeRegExp = "(?m)(?<=\n\"[\\p{javaLowerCase}|\\p{javaUpperCase}|_]{1,10} )[\\d|\\p{javaLowerCase}|\\p{javaUpperCase}]+(?= )";
		String roomRegExp = "(?m)(?<=\n\"[\\p{javaLowerCase}|\\p{javaUpperCase}|_]{1,10} [\\p{javaLowerCase}|\\p{javaUpperCase}|_]{1,10} )"
				+ "[\\d|\\p{javaLowerCase}|\\p{javaUpperCase}|_|-]+(?= )";
		String inputText;
		ArrayList<Lesson> lessons = new ArrayList<Lesson>();

		ArrayList<String> dates = new ArrayList<String>();
		ArrayList<String> times = new ArrayList<String>();
		ArrayList<String> subjects = new ArrayList<String>();
		ArrayList<String> lessonTypes = new ArrayList<String>();
		ArrayList<String> rooms = new ArrayList<String>();

		inputText = load(fileName);

		Pattern patternDate = Pattern.compile(dateRegExp);
		Matcher matcherDate = patternDate.matcher(inputText);

		Pattern patternTime = Pattern.compile(timeRegExp);
		Matcher matcherTime = patternTime.matcher(inputText);

		Pattern patternSubject = Pattern.compile(subjectRegExp);
		Matcher matcherSubject = patternSubject.matcher(inputText);

		Pattern patternLessonType = Pattern.compile(lessonTypeRegExp);
		Matcher matcherLessonType = patternLessonType.matcher(inputText);

		Pattern patternRoom = Pattern.compile(roomRegExp);
		Matcher matcherRoom = patternRoom.matcher(inputText);

		while (matcherDate.find()) {
			String date = matcherDate.group();
			dates.add(date);
			matcherDate.find();
			matcherDate.find();
		}
		while (matcherTime.find()) {
			String time = matcherTime.group();
			times.add(time);
			matcherTime.find();
			matcherTime.find();
		}
		while (matcherSubject.find()) {
			String subject = matcherSubject.group();
			subjects.add(subject);
		}
		while (matcherLessonType.find()) {
			String lessonType = matcherLessonType.group();
			lessonTypes.add(lessonType);
		}
		while (matcherRoom.find()) {
			String room = matcherRoom.group();
			rooms.add(room);
		}

		for (int i = 0; i < dates.size(); i++) {
			lessons.add(new Lesson(dates.get(i), times.get(i), subjects.get(i),
					lessonTypes.get(i), rooms.get(i)));
		}

		Lesson.sortLessons(lessons);

		if (args.length < 2) {
			List<Lesson> smallLessonList = lessons.subList(0,
					LESSONS_TO_PRINT - 1);
			printListOfLessons(smallLessonList);
		} else {
			// Read from Console.
			int i = 0;
			String inputDate;
			int daysToPrint;

			if (args.length > 2) {
				i = 1;
			}
			inputDate = args[i];
			daysToPrint = Integer.parseInt(args[i + 1]);

			i = 0;

			for (; i < lessons.size(); i++) {
				if (Lesson.firstDateEarlierOrEqualsSecond(inputDate, lessons
						.get(i).getDate())) {
					break;
				}
			}
			// Print Lessons starting with index i.
			List<Lesson> smallLessonList = lessons.subList(i,
					lessons.size() - 1);
			printDays(smallLessonList, daysToPrint);
		}
	}
	
	public static String load(String fileName) throws IOException {
		Scanner scanner = new Scanner(new File(fileName), ENCODING);
		StringBuilder result = new StringBuilder();

		while (scanner.hasNextLine()) {
			result.append(scanner.nextLine()).append(System.lineSeparator());
		}
		scanner.close();

		return result.toString();
	}

	private static void printListOfLessons(List<Lesson> inputLessons) {
		String lastDate = inputLessons.get(0).getDate();

		for (int i = 0; (i < LESSONS_TO_PRINT) && (i < inputLessons.size()); i++) {
			if (!inputLessons.get(i).getDate().equals(lastDate)) {
				System.out.println(SEPARATOR);
				lastDate = inputLessons.get(i).getDate();
			}
			System.out.println(inputLessons.get(i).toString());
		}
	}

	private static void printDays(List<Lesson> inputLessons, int inputDays) {
		String lastDate = inputLessons.get(0).getDate();
		int daysCount = 0;

		for (int i = 0; (i < LESSONS_TO_PRINT) && (i < inputLessons.size()); i++) {
			if (!inputLessons.get(i).getDate().equals(lastDate)) {
				daysCount++;
				if (daysCount >= inputDays) {
					break;
				}
				System.out.println(SEPARATOR);
				lastDate = inputLessons.get(i).getDate();
			}
			System.out.println(inputLessons.get(i).toString());
		}
	}
}