package ua.nure.stepko.Task3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Problem3 {
	private static final String FILE_NAME = "3.txt";
	private static final String ENCODING = "Cp1251";
	private static final String STOP = "stop";

	public static void main(String[] args) throws IOException {
		String sourceString = load(FILE_NAME);
		String sourceType = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, ENCODING));
		
		sourceType = reader.readLine();
		while(sourceType != null){
			if(!sourceType.equals(STOP)){
				System.out.println(getCertainElements(sourceType, sourceString));
			}
			else{
				break;
			}			
			sourceType = reader.readLine();
		}
	}

	private static String getCertainElements(String inputType, String inputString)
			throws FileNotFoundException {
		String regexp = "";
		Pattern pattern;
		Matcher matcher;
		StringBuilder result = new StringBuilder();
		
		if(inputType.equals("char")) {
			regexp = "\\b[a-zA-Z�-��-�]\\b";
		}
		else if(inputType.equals("String")) {
			regexp = "\\b[(a-z|A-Z|�-�|�-�)]{2,}\\b";
		}
		else if(inputType.equals("int")) {
			regexp = "(?<=\\s|\\A)[0-9]+(?=\\s|\\Z)";
		}
		else if(inputType.equals("double")) {
			regexp = "[0-9]*\\.[0-9]+\\b";
		}

		pattern = Pattern.compile(regexp);
		matcher = pattern.matcher(inputString);
		while(matcher.find()){
			result.append(matcher.group() + " ");
		}
		return result.toString();
	}

	public static String load(String fileName) throws IOException {
		Scanner scanner = new Scanner(new File(fileName), ENCODING);
		StringBuilder result = new StringBuilder();

		while (scanner.hasNextLine()) {
			result.append(scanner.nextLine()).append(System.lineSeparator());
		}
		scanner.close();

		return result.toString();
	}
}