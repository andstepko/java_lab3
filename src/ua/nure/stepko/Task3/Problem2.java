package ua.nure.stepko.Task3;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class Problem2 {
	private static final String FILE_NAME_1 = "2_1.txt";
	private static final String FILE_NAME_2 = "2_2.txt";
	private static final int NUMBERS_TO_RANDOM = 20;
	private static final int MAX_NUMBER = 50;

	public static void main(String[] args) throws IOException {
		byte[] array;

		fillFirstFile();
		array = readFirstFileAndSort();
		writeSecondFile(array);
	}

	private static void fillFirstFile() throws IOException {
		FileOutputStream fos = new FileOutputStream(FILE_NAME_1);
		byte[] array = new byte[NUMBERS_TO_RANDOM];

		for (int i = 0; i < NUMBERS_TO_RANDOM; i++) {
			Random r = new Random();
			byte curNumber = (byte) r.nextInt(MAX_NUMBER);
			array[i] = curNumber;

			fos.write(new byte[] { curNumber });
		}
		fos.close();

		System.out.print("input  ===> ");
		printArray(array);
		System.out.println();
	}

	private static byte[] readFirstFileAndSort() throws IOException {
		byte[] result = new byte[NUMBERS_TO_RANDOM];
		FileInputStream fis = new FileInputStream(FILE_NAME_1);
		int bytesRead;

		// Fill the result array from the first file.
		bytesRead = fis.read(result, 0, NUMBERS_TO_RANDOM);
		fis.close();

		Arrays.sort(result);

		if(bytesRead != -1){
			return result;
		}
		return new byte[0];
	}

	private static void writeSecondFile(byte[] input) throws IOException {
		FileOutputStream fos = new FileOutputStream(FILE_NAME_2);

		fos.write(input);
		fos.close();

		System.out.print("output ===> ");
		printArray(input);
		System.out.println();
	}

	private static void printArray(byte[] input) {
		for (int i = 0; i < input.length; i++) {
			System.out.print(input[i] + " ");
		}
	}
}