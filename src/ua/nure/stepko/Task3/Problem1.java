package ua.nure.stepko.Task3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Problem1 {

	private static final String FILE_NAME = "1.txt";
	private static final String ENCODING = "Cp1251";

	public static void main(String[] args) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(FILE_NAME), ENCODING);
		StringBuilder result = new StringBuilder();
		StringBuilder resultWord;
		String readWord;

		while (scanner.hasNext()) {
			readWord = scanner.next();
			resultWord = new StringBuilder();
			if (readWord.length() > 2) {
				// Start changing into uppercase.
				for (int i = 0; i < readWord.length(); i++) {
					char c = readWord.charAt(i);

					if (isLowerCase(c)) {
						// Change particular lowercase char with its uppercase
						// analog.
						if ((int) c < (int) 'z') {
							// Letter is Latin.
							resultWord.append((char) (c - 32));
						} else {
							// Letter is Cyrillic
							resultWord.append((char) (c - 32));
						}
					} else {
						// Letter is already uppercase.
						resultWord.append(c);
					}
				}
			} else {
				// Word is too short to change.
				resultWord.append(readWord);
			}
			result.append(resultWord);
			result.append(" ");
		}
		scanner.close();

		System.out.println(result);
	}

	private static boolean isLowerCase(char inputChar) {
		return ((inputChar >= 'a') && (inputChar <= 'z'))
				|| ((inputChar >= '�') && (inputChar <= '�'));
	}
}
