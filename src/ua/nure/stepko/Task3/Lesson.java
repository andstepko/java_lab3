package ua.nure.stepko.Task3;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Lesson {
	private static final String ARROW = "==>";
	private static final String DASH = "-";
	private static final String SPACE = " ";
	
	private String date;
	private String time;
	private String subject;
	private String lessonType;
	private String room;

	public Lesson(String inputDate, String inputTime, String inputSubject, String inputLessonType, String inputRoom){
		date = normalizeDate(inputDate);
		time = normalizeTime(inputTime);
		subject = inputSubject;
		lessonType = inputLessonType;
		room = inputRoom;
	}

	public String getDate(){
		return date;
	}
	
	public int getYear(){
		return Integer.valueOf(Problem5.getCertainGroupSplitedBy(date, "-", 0));
	}
	public int getMonth(){
		return Integer.valueOf(Problem5.getCertainGroupSplitedBy(date, "-", 1));
	}
	public int getDay(){
		return Integer.valueOf(Problem5.getCertainGroupSplitedBy(date, "-", 2));
	}
	
	private static class MyComparer implements Comparator<Lesson>, Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public int compare(Lesson o1, Lesson o2) {
			for (int i = 0; i < o1.date.length(); i++){
				if(o1.date.charAt(i) > o2.date.charAt(i)){
					return 1;
				}
				if(o1.date.charAt(i) < o2.date.charAt(i)){
					return -1;
				}
			}
			return 0;
		}
		
	}

	public static void sortLessons(List<Lesson> inputList){
		Collections.sort(inputList, new MyComparer());
	}
	
	public static boolean firstDateEarlierOrEqualsSecond(String date1, String date2){
		for (int i = 0; i < date1.length(); i++){
			if(date1.charAt(i) < date2.charAt(i)){
				return true;
			}
			if(date1.charAt(i) > date2.charAt(i)){
				return false;
			}
		}		
		return true;
	}
	
	public String toString(){
		StringBuilder result = new StringBuilder();
		
		result.append(date + SPACE + ARROW + SPACE);
		result.append(time + SPACE);
		result.append(subject + SPACE);
		result.append(lessonType + SPACE);
		result.append(room + SPACE);
		
		return result.toString();
	}	
	
	private static String normalizeTime(String inputTime){
		return inputTime.substring(0, inputTime.length() - 3);
	}
	
	private static String normalizeDate(String inputDate){
		List<String> subStrings = Problem5.getGroupsSplitedBy(inputDate, "\\.");
		String result;
		
		result = subStrings.get(2) + DASH + subStrings.get(1) + DASH + subStrings.get(0);
		return result;
	}	
}