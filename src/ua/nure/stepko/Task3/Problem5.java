package ua.nure.stepko.Task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Problem5 {
	private  static final String BASE_NAME = "resources";
	private static final String ENCODING = "Cp1251";
	
	public static void main(String[] args) throws IOException{
		List<String> inputStrings;
		
		inputStrings = readWordsSplitedBy(System.lineSeparator());

		for (int i = 0; i < inputStrings.size(); i++) {
			List<String> wordsList = getGroupsSplitedBy(inputStrings.get(i), " ");
			Locale locale = new Locale(wordsList.get(1));
			ResourceBundle rb = ResourceBundle.getBundle(BASE_NAME, locale);
			
			System.out.println(rb.getString(wordsList.get(0)));
		}
	}


	public static void printStrings(List<String> inputList) {
		for (int i = 0; i < inputList.size(); i++) {
			System.out.println(inputList.get(i));
		}
	}

	public static List<String> getSplitedGroups(String inputText, String inputRegexp) {
		Pattern pattern;
		Matcher matcher;
		List<String> result = new ArrayList<String>();

		pattern = Pattern.compile(inputRegexp);
		matcher = pattern.matcher(inputText);

		while (matcher.find()) {
			result.add(matcher.group());
		}
		return result;
	}

	public static List<String> getGroupsSplitedBy(String inputText, String inputSpliter) {
		String regexp = "";

		regexp = "(?<=(" + inputSpliter + "|^)).+?(?=" + inputSpliter + "|$)";

		return getSplitedGroups(inputText, regexp);
	}

	public static String getCertainGroupSplitedBy(String inputText, String inputSpliter, int inputGroupIndex){
		List<String> groups = getGroupsSplitedBy(inputText, inputSpliter);
		
		return groups.get(inputGroupIndex);
	}
	
	public static List<String> readWordsSplitedBy(String inputSpliter) throws IOException{	
		StringBuffer inputString = new StringBuffer();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in, ENCODING));
		String s = null;
		String readString;
		
		s = br.readLine();
		while(s != null){
			inputString.append(s);
			inputString.append(System.lineSeparator());
			s = br.readLine();
		}
			
		readString = inputString.toString();
		
		return getGroupsSplitedBy(readString, inputSpliter);
	}
}